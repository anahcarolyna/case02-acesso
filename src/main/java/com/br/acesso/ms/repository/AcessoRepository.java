package com.br.acesso.ms.repository;

import com.br.acesso.ms.models.Acesso;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AcessoRepository  extends CrudRepository<Acesso, Long> {
    Optional<Acesso> findByPortaAndCliente(long porta, long cliente);
}
