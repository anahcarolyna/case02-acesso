package com.br.acesso.ms.clients.cliente;

import com.br.acesso.ms.exceptions.ServicoClienteOfflineException;
import com.br.acesso.ms.models.Cliente;

public class ClienteFallback implements ClienteClient{

    @Override
    public Cliente getById(Long id) {
        throw new ServicoClienteOfflineException();
    }
}
