package com.br.acesso.ms.clients.cliente;

import com.br.acesso.ms.exceptions.ClienteNaoEncontradoException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404){
            return new ClienteNaoEncontradoException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
