package com.br.acesso.ms.clients.porta;

import com.br.acesso.ms.exceptions.ServicoPortaOfflineException;
import com.br.acesso.ms.models.Porta;

public class PortaFallback implements PortaClient {

    @Override
    public Porta getById(Long id) {
        throw new ServicoPortaOfflineException();
    }
}
