package com.br.acesso.ms.clients.porta;

import com.br.acesso.ms.exceptions.ClienteNaoEncontradoException;
import com.br.acesso.ms.exceptions.PortaNaoEncontradaException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class PortaClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new ErrorDecoder.Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404){
            return new PortaNaoEncontradaException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
