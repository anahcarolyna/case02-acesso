package com.br.acesso.ms.controller;

import com.br.acesso.ms.service.AcessoService;
import com.br.acesso.ms.models.Acesso;
import com.br.acesso.ms.models.dto.AcessoMapper;
import com.br.acesso.ms.models.dto.CriacaoAcessoRequisicao;
import com.br.acesso.ms.models.dto.CriacaoAcessoResposta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class AcessoController {

    @Autowired
    private AcessoService acessoService;

    @Autowired
    private AcessoMapper mapper;

    @PostMapping("/acesso")
    @ResponseStatus(HttpStatus.CREATED)
    public CriacaoAcessoResposta create(@RequestBody @Valid CriacaoAcessoRequisicao criacaoAcessoRequisicao) {
        Acesso acesso = mapper.converterParaAcesso(criacaoAcessoRequisicao);

        return mapper.converterParaAcessoResposta(acessoService.create(acesso));
    }

    @GetMapping("acesso/{cliente_id}/{porta_id}")
    public CriacaoAcessoResposta buscarPorClienteEPorta(@PathVariable long cliente_id,
                                                        @PathVariable long porta_id ) {
        Acesso acessoObjeto = acessoService
                .getByIdClienteAndIdPorta(porta_id, cliente_id);

        acessoService.enviarAoKafka(mapper.converterParaAcessoConsumer(acessoObjeto));

        return mapper.converterParaAcessoResposta(acessoObjeto);
    }

    @DeleteMapping("acesso/{cliente_id}/{porta_id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarAcesso(@PathVariable long cliente_id,
                              @PathVariable long porta_id) {
        acessoService.delete(porta_id, cliente_id);
    }


}
