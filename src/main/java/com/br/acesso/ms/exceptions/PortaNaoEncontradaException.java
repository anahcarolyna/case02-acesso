package com.br.acesso.ms.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Porta não encontrada.")
public class PortaNaoEncontradaException extends  RuntimeException{
}
