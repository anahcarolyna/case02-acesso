package com.br.acesso.ms.models.dto;

import com.br.acesso.ms.models.Acesso;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class AcessoMapper {

    public Acesso converterParaAcesso(CriacaoAcessoRequisicao criacaoAcessoRequisicao) {
        Acesso acesso = new Acesso();
        acesso.setCliente(criacaoAcessoRequisicao.getCliente_id());
        acesso.setPorta(criacaoAcessoRequisicao.getPorta_id());

        return acesso;

       /* AcessoIdentity id = new AcessoIdentity();
        id.setClienteID(criacaoAcessoRequisicao.getCliente_id());
        id.setPortaId(criacaoAcessoRequisicao.getPorta_id());

        Acesso acesso = new Acesso();
        acesso.setId(id);*/

    }

    public CriacaoAcessoResposta converterParaAcessoResposta(Acesso acesso) {
        CriacaoAcessoResposta criacaoAcessoResposta = new CriacaoAcessoResposta();

        criacaoAcessoResposta.setCliente_id(acesso.getCliente());
        criacaoAcessoResposta.setPorta_id(acesso.getPorta());


        return criacaoAcessoResposta;
    }

    public AcessoKafka converterParaAcessoConsumer(Acesso acesso){
        AcessoKafka acessoKafka = new AcessoKafka();
        acessoKafka.setAcessoValido(getRandomBoolean());
        acessoKafka.setCliente_id(acesso.getCliente());
        acessoKafka.setPorta_id(acesso.getPorta());
        acessoKafka.setDataAtualizacao(LocalDate.now());

        return acessoKafka;
    }

    public static boolean getRandomBoolean() {
        return Math.random() < 0.5;
        // I tried another approaches here, still the same result
    }
}
