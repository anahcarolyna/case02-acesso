package com.br.acesso.ms.models;

import javax.persistence.*;

@Entity
@Table
public class Acesso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private long porta;

    private long cliente;

    public Acesso() {
    }

    public Acesso(Long id, long porta, long cliente) {
        this.id = id;
        this.porta = porta;
        this.cliente = cliente;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getPorta() {
        return porta;
    }

    public void setPorta(long porta) {
        this.porta = porta;
    }

    public long getCliente() {
        return cliente;
    }

    public void setCliente(long cliente) {
        this.cliente = cliente;
    }

    /*@EmbeddedId
    private AcessoIdentity id;

    public AcessoIdentity getId() {
        return id;
    }

    public void setId(AcessoIdentity id) {
        this.id = id;
    }*/


}
