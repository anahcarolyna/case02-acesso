package com.br.acesso.ms.models;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class AcessoIdentity implements Serializable {

    @Column(name = "cliente_id")
    private long clienteID;

    @Column(name = "porta_id")
    private long portaId;

    public long getClienteID() {
        return clienteID;
    }

    public void setClienteID(long clienteID) {
        this.clienteID = clienteID;
    }

    public long getPortaId() {
        return portaId;
    }

    public void setPortaId(long portaId) {
        this.portaId = portaId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AcessoIdentity that = (AcessoIdentity) o;
        return Objects.equals(clienteID, that.clienteID) &&
                Objects.equals(portaId, that.portaId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clienteID, portaId);
    }
}
