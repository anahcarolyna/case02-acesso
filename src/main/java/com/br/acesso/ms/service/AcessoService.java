package com.br.acesso.ms.service;

import com.br.acesso.ms.clients.cliente.ClienteClient;
import com.br.acesso.ms.clients.porta.PortaClient;
import com.br.acesso.ms.exceptions.AcessoNaoEncontradoException;
import com.br.acesso.ms.models.dto.AcessoKafka;
import com.br.acesso.ms.repository.AcessoRepository;
import com.br.acesso.ms.models.Acesso;
import com.br.acesso.ms.models.Cliente;
import com.br.acesso.ms.models.Porta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AcessoService {


    @Autowired
    private KafkaTemplate<String, AcessoKafka> producer;

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private PortaClient portaClient;

    public Acesso create(Acesso acesso) {
        Cliente cliente = clienteClient.getById(acesso.getCliente());
        Porta porta = portaClient.getById(acesso.getPorta());

        return acessoRepository.save(acesso);
    }

    public void delete(long porta_id, long cliente_id) {
       Optional<Acesso> acesso = acessoRepository.findByPortaAndCliente(porta_id, cliente_id);

        if(!acesso.isPresent()) {
            throw new AcessoNaoEncontradoException();
        }

         acessoRepository.delete(acesso.get());

    }

    public Acesso getByIdClienteAndIdPorta(long porta_id, long cliente_id) {
       Optional<Acesso> acesso = acessoRepository.findByPortaAndCliente(porta_id, cliente_id);

        if(!acesso.isPresent()) {
            throw new AcessoNaoEncontradoException();
        }

        return acesso.get();
    }

    public void enviarAoKafka(AcessoKafka acessoKafka) {
        producer.send("spec3-ana-carolina-1", acessoKafka);
    }
}
